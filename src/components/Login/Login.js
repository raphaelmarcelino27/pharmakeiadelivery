import React, { Component } from 'react';
import {
    SafeAreaView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    KeyboardAvoidingView,
    Platform,
    Image,
    Alert,
    ActivityIndicator
} from 'react-native';

import { connect } from 'react-redux';
import {
    onInputChange,
    loginUser
} from '../../redux-store/actions/loginAction';

import { Icon } from 'react-native-elements';
import NavigationService from '../../modules/NavigationService';

class Login extends Component {
    setProps = (key, value) => this.props.onInputChange({ key, value });

    loginAuthentication = () => {
        let username = this.props.username;
        let password = this.props.password;

        let usernameValid = false;
        let passwordValid = false;

        username.length > 0 ? (usernameValid = true) : (usernameValid = false);
        password.length > 0 ? (passwordValid = true) : (passwordValid = false);

        if (usernameValid === false && passwordValid === false) {
            Alert.alert('Username and password is empty');
        } else {
            if (usernameValid === false) {
                Alert.alert('Email cannot be null');
            }

            if (passwordValid === false) {
                Alert.alert('Password cannot be null');
            }

            if (usernameValid && passwordValid) {
                this.props.loginUser(username, password);
            }
        }
    };

    render() {
        return (
            <SafeAreaView style={styles.safeAreaViewStyle}>
                <KeyboardAvoidingView
                    style={styles.viewStyle}
                    behavior={Platform.OS === 'ios' ? 'padding' : null}
                >
                    <Image
                        style={styles.imageStyle}
                        source={require('../../assets/images/Logo.png')}
                    />
                    <View style={styles.textInputViewStyle}>
                        <Icon
                            style={styles.iconStyle}
                            name="user"
                            type="font-awesome"
                            color={'gray'}
                            size={25}
                            onPress={() => console.log('hello')}
                        />
                        <TextInput
                            value={this.props.username}
                            style={styles.textInputStyle}
                            placeholder={'Username'}
                            placeholderTextColor="gray"
                            autoCapitalize={'none'}
                            onChangeText={username =>
                                this.setProps('username', username)
                            }
                        />
                    </View>
                    <View style={styles.textInputViewStyle}>
                        <Icon
                            style={styles.iconStyle}
                            name="lock"
                            type="font-awesome"
                            color={'gray'}
                            size={25}
                            onPress={() => console.log('hello')}
                        />
                        <TextInput
                            secureTextEntry
                            style={styles.textInputStyle}
                            placeholder={'Password'}
                            placeholderTextColor="gray"
                            autoCapitalize={'none'}
                            onChangeText={password => {
                                this.setProps('password', password);
                            }}
                        />
                    </View>

                    <TouchableOpacity
                        onPress={() => {
                            this.loginAuthentication();
                        }}
                        style={styles.buttonStyle}
                    >
                        {this.props.loading === true ? (
                            <ActivityIndicator size="small" color="white" />
                        ) : (
                            <Text style={styles.buttonTextStyle}>Login</Text>
                        )}
                    </TouchableOpacity>
                </KeyboardAvoidingView>
                <TouchableOpacity style={styles.termsAndConditionButtonStyle}>
                    <Text style={styles.termsAndConditionTextStyle}>
                        Terms and Conditions
                    </Text>
                </TouchableOpacity>
            </SafeAreaView>
        );
    }
}

const styles = {
    safeAreaViewStyle: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5F5F5'
    },
    viewStyle: {
        flex: 0.9,
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconStyle: {
        flex: 0.3
    },
    textInputStyle: {
        flex: 0.7,
        color: 'black',
        fontSize: 17,
        fontFamily: 'Roboto-Regular'
    },
    textInputViewStyle: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '80%',
        borderRadius: 40,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        shadowOffset: { width: 0.5, height: 0.5 },
        shadowOpacity: 0.2,
        marginBottom: 10
    },
    buttonStyle: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        width: '80%',
        borderRadius: 40,
        backgroundColor: '#1fa5f2',
        marginTop: 10
    },
    buttonTextStyle: {
        color: 'white',
        fontSize: 17,
        fontFamily: 'Roboto-Regular'
    },
    termsAndConditionButtonStyle: {
        flex: 0.1,
        justifySelf: 'flex-end',
        justifyContent: 'center',
        alignItems: 'center'
    },
    termsAndConditionTextStyle: {
        color: 'gray',
        fontSize: 12
    },
    imageStyle: {
        height: 200,
        width: 300
    }
};

const mapStateToProps = state => {
    return state.login;
};

const mapDispatchToProps = dispatch => {
    return {
        onInputChange: inputObj => dispatch(onInputChange(inputObj)),
        loginUser: (username, password) =>
            dispatch(loginUser(username, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
