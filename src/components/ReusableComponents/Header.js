import React, { Component } from 'react';

import {
    View,
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    Text,
    StatusBar
} from 'react-native';
import { Icon } from 'react-native-elements';

class Header extends Component {
    render() {
        switch (this.props.headerType) {
            case 'normal':
                return (
                    <SafeAreaView style={styles.safeAreaViewStyle}>
                        <StatusBar
                            backgroundColor="#1fa5f2"
                            barStyle="light-content"
                        />
                        <View style={styles.headerViewStyle}>
                            <TouchableOpacity
                                style={styles.iconStyle}
                                onPress={this.props.openDrawer}
                            >
                                <Icon
                                    name="bars"
                                    type="font-awesome"
                                    size={20}
                                    color="white"
                                />
                            </TouchableOpacity>
                            <Text style={styles.titleStyle}>
                                {this.props.title}
                            </Text>
                        </View>
                        {this.props.children}
                    </SafeAreaView>
                );
                break;

            case 'back':
                return (
                    <SafeAreaView style={styles.safeAreaViewStyle}>
                        <StatusBar
                            backgroundColor="#1fa5f2"
                            barStyle="light-content"
                        />
                        <View style={styles.headerViewStyle}>
                            <TouchableOpacity
                                style={styles.iconStyle}
                                onPress={this.props.onPress}
                            >
                                <Icon
                                    name="chevron-left"
                                    type="font-awesome"
                                    size={20}
                                    color="white"
                                />
                            </TouchableOpacity>
                            <Text style={styles.titleStyle}>
                                {this.props.title}
                            </Text>
                        </View>
                        {this.props.children}
                    </SafeAreaView>
                );
                break;
            default:
                break;
        }
    }
}

const styles = StyleSheet.create({
    safeAreaViewStyle: {
        flex: 1
    },
    iconStyle: {
        position: 'absolute',
        left: 0,
        top: 0,
        paddingVertical: 20,
        paddingHorizontal: 15
    },
    titleStyle: {
        fontFamily: 'Roboto-Regular',
        fontSize: 18,
        color: 'white'
    },
    headerViewStyle: {
        height: 60,
        backgroundColor: '#1fa5f2',
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'black',
        shadowOpacity: 0.2,
        elevation: 3
    }
});

export default Header;
