import React, { Component } from 'react';

import {
    View,
    Text,
    FlatList,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import Header from '../ReusableComponents/Header';
import { SearchBar, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';
import NavigationService from '../../modules/NavigationService';
import {
    onInputChange,
    getOrders
} from '../../redux-store/actions/deliveryHistoryAction';
import moment from 'moment';

class DeliveryHistory extends Component {
    setProps = (key, value) => this.props.onInputChange({ key, value });

    renderProduct = ({ item }) => {
        console.log(item);
        const { orderNo, date, payment, orders, status, address, total } = item;

        return (
            <TouchableOpacity
                onPress={() => {
                    NavigationService.navigate('Order', { data: item });
                }}
                style={styles.buttonStyle}
            >
                <View>
                    <Text style={styles.orderNoTextStyle}>{orderNo}</Text>
                    <View style={styles.paymentViewStyle}>
                        <Text style={styles.paymentViewText1}>Payment - </Text>
                        <Text style={styles.paymentViewText2}>
                            {payment.toUpperCase()}
                        </Text>
                    </View>
                    <View style={styles.paymentViewStyle}>
                        <Text style={styles.paymentViewText1}>Status - </Text>
                        <Text
                            style={[
                                styles.paymentViewText2,
                                {
                                    color:
                                        status === 'received' ? 'green' : 'red'
                                }
                            ]}
                        >
                            {status.toUpperCase()}
                        </Text>
                    </View>
                    <View style={styles.paymentViewStyle}>
                        <Text style={styles.paymentViewText1}>Date - </Text>
                        <Text
                            style={[
                                styles.paymentViewText2,
                                { color: 'black' }
                            ]}
                        >
                            {moment(date).format('MMMM Do YYYY')}
                        </Text>
                    </View>
                    <View style={styles.paymentViewStyle}>
                        <Text style={styles.paymentViewText1}>Amount - </Text>
                        <Text
                            style={[
                                styles.paymentViewText2,
                                { color: 'black' }
                            ]}
                        >
                            {`₱ ${total.toFixed(2)}`}
                        </Text>
                    </View>
                    <View style={styles.paymentViewStyle}>
                        <Text
                            style={[
                                styles.paymentViewText1,
                                {
                                    color: 'black',
                                    fontFamily: 'Roboto-Bold'
                                }
                            ]}
                        >
                            Address -{' '}
                        </Text>
                        <Text
                            style={[
                                styles.paymentViewText2,
                                {
                                    flexShrink: 1,
                                    color: 'black'
                                }
                            ]}
                        >
                            {address}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    render() {
        return (
            <Header
                headerType={'normal'}
                title={'Delivery History'}
                openDrawer={() => this.props.navigation.openDrawer()}
            >
                <NavigationEvents
                    onWillFocus={() => {
                        this.props.getOrders(
                            moment().format('YYYY-MM-DD'),
                            this.props.searchValue
                        );
                    }}
                />
                <View style={styles.viewStyle}>
                    <DatePicker
                        style={{
                            width: '15%',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        customStyles={{
                            dateInput: {
                                display: 'none'
                            },
                            dateIcon: {}
                        }}
                        onDateChange={date => {
                            this.setProps('date', date);
                        }}
                        confirmBtnText={'Confirm'}
                        cancelBtnText={'Cancel'}
                    />
                    <SearchBar
                        containerStyle={styles.searchBarContainerStyle}
                        inputContainerStyle={
                            styles.searchBarInputContainerStyle
                        }
                        onChangeText={text => {
                            this.setProps('searchValue', text);
                        }}
                        placeholder={'Search Order Number'}
                        value={this.props.searchValue}
                        showCancel={false}
                        clearIcon={false}
                        searchIcon={false}
                    />
                    <TouchableOpacity
                        style={{
                            width: '10%',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                        onPress={() => {
                            this.props.getOrders(
                                this.props.date,
                                this.props.searchValue
                            );
                        }}
                    >
                        <Icon name="search" type="font-awesome" />
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={this.props.orders}
                    renderItem={this.renderProduct}
                    showsVerticalScrollIndicator={false}
                />
            </Header>
        );
    }
}

const styles = StyleSheet.create({
    viewStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchBarContainerStyle: {
        width: '75%',
        backgroundColor: 'transparent',
        borderBottomColor: 'transparent',
        borderTopColor: 'transparent',
        padding: 5
    },
    searchBarInputContainerStyle: {
        backgroundColor: 'white',
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'black',
        shadowOpacity: 0.2,
        elevation: 3
    },
    buttonStyle: {
        width: '100%',
        backgroundColor: 'white',
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'black',
        shadowOpacity: 0.2,
        elevation: 3,
        padding: 20,
        marginBottom: 5
    },
    orderNoTextStyle: {
        fontFamily: 'Roboto-Light',
        fontWeight: 'bold',
        fontSize: 20
    },
    paymentViewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 1
    },
    paymentViewText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 15
    },
    paymentViewText2: { fontSize: 15 },
    dateTodayStyle: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const mapStateToProps = state => {
    return state.deliveryHistory;
};

const mapDispatchToProps = dispatch => {
    return {
        onInputChange: inputObj => dispatch(onInputChange(inputObj)),
        getOrders: (date, searchValue) => dispatch(getOrders(date, searchValue))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryHistory);
