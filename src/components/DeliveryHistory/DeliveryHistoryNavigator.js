import {
    createStackNavigator,
    TransitionPresets
} from 'react-navigation-stack';
import DeliveryHistory from './DeliveryHistory';
import Order from '../Home/Order';

const DeliveryHistoryNavigator = createStackNavigator(
    {
        DeliveryHistory: DeliveryHistory,
        Order: Order
    },
    {
        headerMode: 'none',
        defaultNavigationOptions: {
            ...TransitionPresets.SlideFromRightIOS
        }
    }
);

export default DeliveryHistoryNavigator;
