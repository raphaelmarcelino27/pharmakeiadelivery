import React, { Component } from 'react';
import {
    TouchableOpacity,
    StyleSheet,
    Text,
    FlatList,
    View
} from 'react-native';
import moment from 'moment';

import Header from '../ReusableComponents/Header';
import { connect } from 'react-redux';
import {
    onInputChange,
    getDeliveries
} from '../../redux-store/actions/homeAction';
import { NavigationEvents } from 'react-navigation';

import NavigationService from '../../modules/NavigationService';

class Home extends Component {
    setProps = (key, value) => this.props.onInputChange({ key, value });

    renderProduct = ({ item }) => {
        console.log(item);
        const { orderNo, date, payment, orders, status, address, total } = item;

        return (
            <TouchableOpacity
                onPress={() => {
                    NavigationService.navigate('Order', { data: item });
                }}
                style={styles.buttonStyle}
            >
                <View>
                    <Text style={styles.orderNoTextStyle}>{orderNo}</Text>
                    <View style={styles.paymentViewStyle}>
                        <Text style={styles.paymentViewText1}>Payment - </Text>
                        <Text style={styles.paymentViewText2}>
                            {payment.toUpperCase()}
                        </Text>
                    </View>
                    <View style={styles.paymentViewStyle}>
                        <Text style={styles.paymentViewText1}>Status - </Text>
                        <Text
                            style={[
                                styles.paymentViewText2,
                                {
                                    color:
                                        status === 'for delivery'
                                            ? 'red'
                                            : 'green'
                                }
                            ]}
                        >
                            {status.toUpperCase()}
                        </Text>
                    </View>
                    <View style={styles.paymentViewStyle}>
                        <Text style={styles.paymentViewText1}>Date - </Text>
                        <Text style={styles.paymentViewText2}>
                            {moment(date).format('MMMM Do YYYY')}
                        </Text>
                    </View>
                    <View style={styles.paymentViewStyle}>
                        <Text style={styles.paymentViewText1}>Amount - </Text>
                        <Text style={styles.paymentViewText2}>
                            {`₱ ${total.toFixed(2)}`}
                        </Text>
                    </View>
                    <View style={styles.paymentViewStyle}>
                        <Text style={styles.paymentViewText1}>Address - </Text>
                        <Text
                            style={[
                                styles.paymentViewText2,
                                {
                                    flexShrink: 1,
                                    color: 'black'
                                }
                            ]}
                        >
                            {address}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    handleRefresh = async () => {
        this.props.getDeliveries();
    };

    render() {
        return (
            <Header
                headerType={'normal'}
                title={'Deliveries'}
                openDrawer={() => this.props.navigation.openDrawer()}
            >
                <NavigationEvents
                    onWillFocus={() => {
                        console.log('aslkdj');
                        this.props.getDeliveries();
                    }}
                />
                <View style={styles.dateTodayStyle}>
                    <Text>{moment().format('MMMM Do YYYY dddd')}</Text>
                </View>
                {this.props.data.length > 0 ? (
                    <FlatList
                        data={this.props.data}
                        renderItem={this.renderProduct}
                        showsVerticalScrollIndicator={false}
                        refreshing={this.props.refreshing}
                        onRefresh={this.handleRefresh}
                    />
                ) : (
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Text>No order to be delivered</Text>
                    </View>
                )}
            </Header>
        );
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        width: '100%',
        backgroundColor: 'white',
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'black',
        shadowOpacity: 0.2,
        elevation: 3,
        padding: 20,
        marginBottom: 5
    },
    orderNoTextStyle: {
        fontFamily: 'Roboto-Light',
        fontWeight: 'bold',
        fontSize: 20
    },
    paymentViewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 1
    },
    paymentViewText1: {
        fontFamily: 'Roboto-Bold',
        fontSize: 15
    },
    paymentViewText2: { fontSize: 15 },
    dateTodayStyle: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const mapStateToProps = state => {
    return state.home;
};

const mapDispatchToProps = dispatch => {
    return {
        onInputChange: inputObj => dispatch(onInputChange(inputObj)),
        getDeliveries: () => dispatch(getDeliveries())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
