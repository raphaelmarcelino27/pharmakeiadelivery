import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity
} from 'react-native';
import Header from '../ReusableComponents/Header';
import {
    onInputChange,
    updateOrders
} from '../../redux-store/actions/orderAction';
import { connect } from 'react-redux';

class Order extends Component {
    setProps = (key, value) => this.props.onInputChange({ key, value });

    renderOrders = ({ item }) => {
        const {
            product: { productTitle = '', image = '', productNo = '' },
            price,
            quantity
        } = item;

        return (
            <View style={styles.orderView}>
                <View style={styles.imageView}>
                    <Image
                        style={styles.imageStyle}
                        source={{
                            uri: `http://pharmakeia.herokuapp.com/${image}`
                        }}
                    />
                </View>
                <View style={styles.orderContentView}>
                    <Text style={styles.orderContentProductTitle}>
                        {productTitle}
                    </Text>
                    <View style={styles.orderContentAmount}>
                        <Text>Amount - </Text>
                        <Text>{`₱ ${price.toFixed(2)}`}</Text>
                        <Text>{` (Qty: ${quantity})`}</Text>
                    </View>
                    <View style={styles.orderContentAmount}>
                        <Text>Product No: - </Text>
                        <Text>{productNo}</Text>
                    </View>
                </View>
            </View>
        );
    };
    render() {
        const data = this.props.navigation.getParam('data');
        const {
            orderNo,
            orders,
            address,
            _id,
            total,
            status,
            customer: { firstName, lastName }
        } = data;

        const fullName = `${firstName} ${lastName}`;
        console.log(data);
        return (
            <Header
                headerType="back"
                title={'Orders'}
                onPress={() => this.props.navigation.goBack()}
            >
                <View style={styles.orderNoView}>
                    <Text style={styles.orderNoStaticText}>Order #:</Text>
                    <Text style={styles.orderNoText}>{orderNo}</Text>
                </View>
                <View style={styles.orderNoView}>
                    <Text style={styles.orderNoStaticText}>Name :</Text>
                    <Text
                        style={[
                            styles.orderNoText,
                            { fontFamily: 'Roboto-Regular' }
                        ]}
                    >
                        {fullName}
                    </Text>
                </View>
                <View style={styles.orderNoView}>
                    <Text style={styles.orderNoStaticText}>Address :</Text>
                    <Text
                        style={[
                            styles.orderNoText,
                            { fontFamily: 'Roboto-Regular' }
                        ]}
                    >
                        {address}
                    </Text>
                </View>
                <View style={styles.orderNoView}>
                    <Text style={styles.orderNoStaticText}>Total Price :</Text>
                    <Text
                        style={[
                            styles.orderNoText,
                            { fontFamily: 'Roboto-Regular' }
                        ]}
                    >
                        {`₱ ${total.toFixed(2)}`}
                    </Text>
                </View>
                <Text style={styles.itemListText}>Item List</Text>
                <FlatList
                    data={orders}
                    renderItem={this.renderOrders}
                    showsVerticalScrollIndicator={false}
                />
                {status !== 'received' && (
                    <View style={styles.buttonView}>
                        <TouchableOpacity
                            disabled={this.props.buttonLoading}
                            onPress={() => {
                                this.props.updateOrders('received', _id);
                            }}
                            style={styles.buttonStyle}
                        >
                            <Text style={styles.buttonText}>DELIVERED</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            disabled={this.props.buttonLoading}
                            onPress={() => {
                                this.props.updateOrders('for approval', _id);
                            }}
                            style={[
                                styles.buttonStyle,
                                { backgroundColor: '#fc1e49' }
                            ]}
                        >
                            <Text style={styles.buttonText}>DECLINED</Text>
                        </TouchableOpacity>
                    </View>
                )}
            </Header>
        );
    }
}

const styles = StyleSheet.create({
    orderNoView: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 10,
        marginBottom: 5
    },
    orderNoStaticText: {
        fontSize: 16,
        marginRight: 5,
        fontFamily: 'Roboto-Bold'
    },
    orderNoText: {
        fontSize: 16,
        fontFamily: 'Roboto-Bold',
        flexShrink: 1
    },
    itemListText: {
        fontSize: 14,
        fontFamily: 'Roboto-Light',
        paddingHorizontal: 10,
        marginTop: 10
    },
    itemListView: {
        flex: 1,
        backgroundColor: 'white',
        marginVertical: 20
    },
    buttonView: {
        height: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonStyle: {
        width: '95%',
        backgroundColor: '#26a9fc',
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
        marginBottom: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 15
    },
    orderView: { flexDirection: 'row' },
    imageView: {
        flex: 0.3,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5
    },
    orderContentView: {
        flex: 0.7,
        borderBottomWidth: 1,
        borderColor: 'gray',
        padding: 10
    },
    imageStyle: {
        height: 80,
        width: 80,
        borderRadius: 10,
        borderColor: 'gray'
    },
    orderContentProductTitle: {
        fontFamily: 'Roboto-Bold',
        fontSize: 16
    },
    orderContentAmount: { flexDirection: 'row' }
});

const mapStateToProps = state => {
    return state.order;
};

const mapDispatchToProps = dispatch => {
    return {
        onInputChange: inputObj => dispatch(onInputChange(inputObj)),
        updateOrders: (status, orderId) =>
            dispatch(updateOrders(status, orderId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Order);
