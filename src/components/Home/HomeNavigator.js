import { createStackNavigator } from 'react-navigation-stack';
import Home from './Home';
import Order from './Order';
import { TransitionPresets } from 'react-navigation-stack';
const HomeNavigator = createStackNavigator(
    {
        Home: Home,
        Order: Order
    },
    {
        initialRouteName: 'Home',
        headerMode: 'none',
        defaultNavigationOptions: {
            ...TransitionPresets.SlideFromRightIOS
        }
    }
);

export default HomeNavigator;
