import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import React from 'react';
import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Text,
    SafeAreaView,
    ScrollView
} from 'react-native';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

import HomeNavigator from './Home/HomeNavigator';
import DeliveryHistoryNavigator from './DeliveryHistory/DeliveryHistoryNavigator';
import Login from './Login/Login';
import NavigationService from '../modules/NavigationService';

const Logo = props => {
    return (
        <SafeAreaView style={styles.safeAreaViewStyle}>
            <View style={styles.containerStyle}>
                <Image
                    style={styles.imageStyle}
                    source={require('../assets/images/Logo.png')}
                />
            </View>
            <ScrollView contentContainerStyle={{ alignItems: 'center' }}>
                <DrawerItems {...props} />
                <TouchableOpacity
                    onPress={() => {
                        NavigationService.navigate('Login');
                    }}
                    style={{
                        width: '100%',
                        marginTop: 5,
                        justifyContent: 'center',
                        flexDirection: 'row'
                    }}
                >
                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
                        Logout
                    </Text>
                </TouchableOpacity>
            </ScrollView>
        </SafeAreaView>
    );
};

const DrawerNav = createDrawerNavigator(
    {
        Deliveries: {
            screen: HomeNavigator,
            navigationOptions: {
                drawerIcon: ({ tintColor }) => (
                    <Icon
                        name="truck"
                        type="font-awesome"
                        color={tintColor}
                        size={20}
                    />
                )
            }
        },
        'Delivery History': {
            screen: DeliveryHistoryNavigator,
            navigationOptions: {
                drawerIcon: ({ tintColor }) => (
                    <Icon
                        name="history"
                        type="font-awesome"
                        color={tintColor}
                        size={20}
                    />
                )
            }
        }
    },
    {
        initialRouteName: 'Deliveries',
        contentComponent: Logo,
        drawerWidth: 200,
        contentOptions: {
            labelStyle: {
                fontFamily: 'Roboto-Regular'
            }
        }
    }
);

const styles = StyleSheet.create({
    safeAreaViewStyle: {
        flex: 1
    },
    containerStyle: {
        height: 150,
        paddingVertical: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageStyle: { height: 150, width: 150 }
});

export default DrawerNav;
