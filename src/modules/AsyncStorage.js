import AsyncStorage from '@react-native-community/async-storage';

const AppStorage = {
    set: (key, value) => {
        return new Promise((resolve, reject) => {
            AsyncStorage.setItem(
                key,
                typeof value === 'object' ? JSON.stringify(value) : value
            ).catch(err => reject(err));
        });
    },

    get: key => {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(key)
                .then(value => resolve(value))
                .catch(err => reject(err));
        });
    },

    getObj: key => {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(key)
                .then(value => resolve(JSON.parse(value)))
                .catch(err => reject(err));
        });
    },

    getAll: () => {
        return new Promise((resolve, reject) => {
            AsyncStorage.getAllKeys()
                .then(keys => AsyncStorage.multiGet(keys))
                .then(values => {
                    const obj = {};

                    values.forEach(([key, value]) => {
                        obj[key] = value;
                    });

                    resolve(obj);
                })
                .catch(err => reject(err));
        });
    },

    clearItem: key => {
        return new Promise((resolve, reject) => {
            AsyncStorage.removeItem(key)
                .then(value => resolve(value))
                .catch(err => reject(err));
        });
    },

    clear: () => {
        return new Promise((resolve, reject) => {
            AsyncStorage.getAllKeys()
                .then(keys => AsyncStorage.multiRemove(keys))
                .then(values => resolve(values))
                .catch(err => reject(err));
        });
    }
};

export default AppStorage;
