import { NavigationActions, StackActions } from 'react-navigation';

let navigator;

function setTopLevelNavigator(navigatorRef) {
    navigator = navigatorRef;
}

function navigate(routeName, params) {
    navigator.dispatch(
        // NavigationActions.res
        NavigationActions.navigate({
            routeName,
            params
        })
    );
}

function goBack() {
    navigator.dispatch(NavigationActions.back());
}

function reset(routeName) {
    navigator.dispatch(
        StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName })]
        })
    );
}

function replace(routeName) {
    navigator.dispatch(StackActions.replace({ routeName }));
}

function popToTop() {
    navigator.dispatch(StackActions.popToTop());
}

// add other navigation functions that you need and export them

export default {
    navigate,
    replace,
    setTopLevelNavigator,
    goBack,
    reset,
    popToTop
};
