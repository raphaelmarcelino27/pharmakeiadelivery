import { createAppContainer } from 'react-navigation';
import {
    createStackNavigator,
    TransitionPresets
} from 'react-navigation-stack';
import { Easing, Animated } from 'react-native';
import Login from './components/Login/Login';
import DrawerNav from './components/DrawerNav';

const AppNavigator = createStackNavigator(
    {
        Login: Login,
        MainApp: DrawerNav
    },
    {
        initialRouteName: 'Login',
        headerMode: 'none',
        defaultNavigationOptions: {
            ...TransitionPresets.SlideFromRightIOS
        }
    }
);

const MainNavigator = createAppContainer(AppNavigator);

export default createAppContainer(MainNavigator);
