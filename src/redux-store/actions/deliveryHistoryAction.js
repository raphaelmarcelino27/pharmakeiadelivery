import axios from 'axios';
import NavigationService from '../../modules/NavigationService';
import { DELIVERY_HISTORY_CHANGED } from '../actions/actionTypes';
import AppStorage from '../../modules/AsyncStorage';
import moment from 'moment';

export const onInputChange = ({ key, value }) => {
    return {
        type: DELIVERY_HISTORY_CHANGED,
        payload: { key, value }
    };
};

const setState = (dispatch, key, value) => {
    dispatch({
        type: DELIVERY_HISTORY_CHANGED,
        payload: { key, value }
    });
};

export const getOrders = (date, searchValue) => {
    if (!date) {
        date = moment().format('YYYY-MM-DD');
    }
    return async dispatch => {
        let user = await AppStorage.get('user');
        user = JSON.parse(user);
        let id = user._id;
        console.log(id);
        console.log(date);
        console.log(searchValue);
        axios
            .get(
                `http://pharmakeia.herokuapp.com/deliverymen/${id}/delivered?search=${searchValue}`
            )
            .then(res => {
                console.log(res);
                setState(dispatch, 'orders', res.data);
            })
            .catch(error => {
                console.log(error);
            });
    };
};
