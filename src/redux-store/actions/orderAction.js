import axios from 'axios';
import NavigationService from '../../modules/NavigationService';
import { ORDER_CHANGED } from '../actions/actionTypes';
import AppStorage from '../../modules/AsyncStorage';
import { Alert } from 'react-native';
import moment from 'moment';

export const onInputChange = ({ key, value }) => {
    return {
        type: ORDER_CHANGED,
        payload: { key, value }
    };
};

const setState = (dispatch, key, value) => {
    dispatch({
        type: ORDER_CHANGED,
        payload: { key, value }
    });
};

export const updateOrders = (status, orderId) => {
    return async dispatch => {
        setState(dispatch, 'buttonLoading', true);
        console.log(status, orderId);
        const dateDelivered = moment().toISOString();
        axios
            .put(`http://pharmakeia.herokuapp.com/order/${orderId}`, {
                status,
                dateDelivered
            })
            .then(res => {
                if (res) {
                    setState(dispatch, 'buttonLoading', false);
                }
                if (status === 'for approval') {
                    Alert.alert('Order Delivered!');
                } else {
                    Alert.alert('Order Received!');
                }
                NavigationService.goBack();
            })
            .catch(err => {
                console.log(err);
            });
    };
};
