import axios from 'axios';
import NavigationService from '../../modules/NavigationService';
import { LOGIN_CHANGED } from '../actions/actionTypes';
import AppStorage from '../../modules/AsyncStorage';
import { Alert } from 'react-native';

export const onInputChange = ({ key, value }) => {
    return {
        type: LOGIN_CHANGED,
        payload: { key, value }
    };
};

const setState = (dispatch, key, value) => {
    dispatch({
        type: LOGIN_CHANGED,
        payload: { key, value }
    });
};

export const loginUser = (username, password) => {
    console.log('Rap');
    let email = username;
    return async dispatch => {
        setState(dispatch, 'loading', true);
        axios
            .post('http://pharmakeia.herokuapp.com/deliverymen/auth', {
                email,
                password
            })
            .then(response => {
                console.log(response);
                const user = response.data;
                AppStorage.set('user', user);

                setState(dispatch, 'loading', false);
                NavigationService.navigate('MainApp', {
                    transition: 'default'
                });
            })
            .catch(error => {
                if (error.response === undefined) {
                    Alert.alert('Please check your internet connection.');
                } else {
                    Alert.alert("Account doesn't exist");
                }
                setState(dispatch, 'loading', false);
            });
    };
};
