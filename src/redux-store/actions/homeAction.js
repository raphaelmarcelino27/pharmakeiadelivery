import axios from 'axios';
import NavigationService from '../../modules/NavigationService';
import { HOME_CHANGED } from '../actions/actionTypes';
import AppStorage from '../../modules/AsyncStorage';

export const onInputChange = ({ key, value }) => {
    return {
        type: HOME_CHANGED,
        payload: { key, value }
    };
};

const setState = (dispatch, key, value) => {
    dispatch({
        type: HOME_CHANGED,
        payload: { key, value }
    });
};

export const getDeliveries = () => {
    return async dispatch => {
        let user = await AppStorage.get('user');

        user = JSON.parse(user);
        let id = user._id;

        axios
            .get(`http://pharmakeia.herokuapp.com/deliverymen/${id}/deliveries`)
            .then(response => {
                console.log(response.data);
                setState(dispatch, 'data', response.data);
            })
            .catch(error => {
                console.log(error);
            });
    };
};
