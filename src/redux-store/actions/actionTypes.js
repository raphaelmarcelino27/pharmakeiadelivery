export const HOME_CHANGED = 'home_changed';

export const LOGIN_CHANGED = 'login_changed';

export const DELIVERY_HISTORY_CHANGED = 'delivery_history_changed';

export const ORDER_CHANGED = 'order_changed';
