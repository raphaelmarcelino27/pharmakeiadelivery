import { HOME_CHANGED } from '../actions/actionTypes';

const INITIAL_STATE = {
    data: [],
    refreshing: false
};

export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action;
    switch (type) {
        case HOME_CHANGED: {
            return { ...state, [payload.key]: payload.value };
        }

        default:
            return state;
    }
};
