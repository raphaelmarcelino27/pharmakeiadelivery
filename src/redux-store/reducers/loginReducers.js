import { LOGIN_CHANGED } from '../actions/actionTypes';

const INITIAL_STATE = {
    username: '',
    password: '',
    loading: false
};

export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action;
    switch (type) {
        case LOGIN_CHANGED: {
            return { ...state, [payload.key]: payload.value };
        }

        default:
            return state;
    }
};
