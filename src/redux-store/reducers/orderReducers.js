import { ORDER_CHANGED } from '../actions/actionTypes';

const INITIAL_STATE = {
    buttonLoading: false
};

export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action;
    switch (type) {
        case ORDER_CHANGED: {
            return { ...state, [payload.key]: payload.value };
        }

        default:
            return state;
    }
};
