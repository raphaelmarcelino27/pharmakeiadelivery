import { combineReducers } from 'redux';

import HomeReducers from '../reducers/homeReducers';
import LoginReducers from '../reducers/loginReducers';
import DeliveryHistoryReducers from './deliveryHistoryReducers';
import OrderReducers from '../reducers/orderReducers';

export default combineReducers({
    home: HomeReducers,
    login: LoginReducers,
    deliveryHistory: DeliveryHistoryReducers,
    order: OrderReducers
});
