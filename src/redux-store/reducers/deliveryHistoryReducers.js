import { DELIVERY_HISTORY_CHANGED } from '../actions/actionTypes';

const INITIAL_STATE = {
    data: [],
    searchValue: '',
    orders: []
};

export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action;
    switch (type) {
        case DELIVERY_HISTORY_CHANGED: {
            return { ...state, [payload.key]: payload.value };
        }

        default:
            return state;
    }
};
